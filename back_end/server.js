const express = require('express')
const app = express()
var Datastore = require('nedb'), 
  UsersDB = new Datastore({ filename: 'db/users', autoload: true }),
  MarkersDB = new Datastore({ filename: 'db/markers', autoload: true });
// You can issue commands right away


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.post('/new_user', function(request, response){
    console.log({action:'new_user', body:request.body});// your JSON
    response.setHeader('Content-Type', 'application/json');
    // response.send(JSON.stringify({user:saveNewUser(request.body.data)}));
    // response.send(JSON.stringify({user:'01010101010'}));
    var doc = {
        name: request.body.data.name,
        pw: request.body.data.pw
    };
    UsersDB.insert(doc, function (err, newDoc) {
        console.log({newDoc:newDoc});
        response.send(JSON.stringify({user:newDoc}));
    });
});

app.listen(8081, function () {
  console.log('Example app listening on port 8081!')
})



// Map Marker JSON
// {
//     "marker_id": 0,
//     "content_type": "url",
//     "artist_name": "The Millenium",
//     "lat_long": "43.0578701, -88.0374691", //location on map
//     "marker_content": {
//         "content_title": "New Song!", //Message from poster
//         "content_url": "http://themilleniumwi.bandcamp.com/album/interstates-single", // mp3, image
//     }
// }

function saveNewUser(data) {
    var doc = {
        name: data.name,
        pw: data.pw
    };
    UsersDB.insert(doc, function (err, newDoc) {
        console.log({newDoc:newDoc});
        return newDoc;
    });
}